sap.ui.define(["sap/ui/core/mvc/Controller",
	"../model/formatter"
], function(Controller, Formatter) {
	"use strict";
	return Controller.extend("com.inspiricon.tspTimeSheetPlanning.controller.Main", {
		/**
		 *@memberOf com.inspiricon.tspTimeSheetPlanning.controller.Main
		 */
		formatter: Formatter,
		onAddPress: function() {
			//This code was generated by the layout editor.

		}
	});
});