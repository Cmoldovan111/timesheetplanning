sap.ui.define([], function() {

	return {

		approveIcon: function(icon) {
			//console.log(icon);
		
			if (icon === "0,000") {
				return "sap-icon://sys-cancel-2";
			} else 	if (icon  === "1,000") {
				return "sap-icon://accept";
			} else {
					return "sap-icon://error";
			}
		},
		approveIconColor: function(icon) {
			console.log(icon);
		
			if (icon === "0,000") {
				return "red";
			} else 	if (icon === "1,000") {
				return "green";
			} else {
					return "black";
			}
		}

	};
});